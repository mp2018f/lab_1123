import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs-compat';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';

/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {

  profileList: Observable<any[]>;
  myuid: String;

  constructor(public navCtrl: NavController, private db: AngularFireDatabase, public navParams: NavParams, private afAuth: AngularFireAuth) {
    console.log(navParams.data.uid);
    this.myuid = navParams.data.uid;
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
    //this.profileList = this.db.list('profiles').valueChanges();
    this.profileList = this.db.list('profiles').snapshotChanges().pipe(map(changes =>  changes.map(c => 		({ key: c.payload.key, ...c.payload.val() }))));
    //this.myuid = this.afAuth.auth.currentUser.uid;
    console.log('myuid='+this.myuid);
  }

}
